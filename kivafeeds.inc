<?php

/**
* Parses My Feed
*/
class kivaParser extends FeedsParser {

  /**
  * Parses a raw string and returns a FeedsParserResult object from it.
  */
  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    // Get the file's content.
    $string = $batch->getRaw();
    $xml = simplexml_load_string($string);
    $items = array();
    foreach ($xml->loans->loan as $loan) {
	    $item = array();
	    
	    //$item['title'] = $loan->name;
	    
	    $item['id'] = $loan->id;
	    $item['name'] = $loan->name;
	    $item['status'] = $loan->status;
	    $item['loan_amount'] = $loan->loan_amount;
	    $item['funded_amount'] = $loan->funded_amount;
	    $item['basket_amount'] = $loan->basket_amount;
	    $item['borrower_count'] = $loan->borrower_count;
	
	    $item['activity'] = $loan->activity;
	    $item['sector'] = $loan->sector;
	    $item['use'] = $loan->use;
	    $item['partner_id'] = $loan->partner_id;
	    $item['posted_date'] = $loan->posted_date;
	
      $items[] = $item;
    }

    // Populate the FeedsImportBatch object with the parsed results.
    $batch->setTitle('Kiva Items');
    $batch->setItems($items);

  }

  public function getMappingSources() {  
    return array(       
      'id' => array(
        'name' => t('id'),
        'description' => t('id'),
      ),
      'name' => array(
        'name' => t('name'),
        'description' => t('name'),
      ),
      'status' => array(
        'name' => t('status'),
        'description' => t('status'),
      ),
      'loan_amount' => array(
        'name' => t('loan_amount'),
        'description' => t('loan_amount'),
      ),
      'funded_amount' => array(
        'name' => t('funded_amount'),
        'description' => t('funded_amount'),
      ),
      'basket_amount' => array(
        'name' => t('basket_amount'),
        'description' => t('basket_amount'),
      ),
      'borrower_count' => array(
        'name' => t('borrower_count'),
        'description' => t('borrower_count'),
      ),
      'activity' => array(
        'name' => t('activity'),
        'description' => t('activity'),
      ),
      'sector' => array(
        'name' => t('sector'),
        'description' => t('sector'),
      ),
      'partner_id' => array(
        'name' => t('partner_id'),
        'description' => t('partner_id'),
      ),
      'posted_date' => array(
        'name' => t('posted_date'),
        'description' => t('posted_date'),
      ),
    );
  }
}