<?php

function kiva_admin_settings() {
  $form['kiva_loans_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Loan block settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE, 
  );
  $form['kiva_loans_block']['kiva_loans_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum loans'),
    '#default_value' => variable_get('kiva_loans_max', 5),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('The maximum number of items to display in the block.'),
    '#required' => TRUE,
  );
  $form['kiva_loans_block']['kiva_loans_rand'] = array(
    '#type' => 'checkbox',
    '#title' => t('Random loans'),
    '#default_value' => variable_get('kiva_loans_rand', 0),
    '#description' => t('Choose random loans. If unchecked, block will show the most recent loans.'),
  );
  $form['kiva_loans_block']['kiva_filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filters'),
    '#description' => t('Narrow your search with these parameters.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE, 
  );
  $form['kiva_loans_block']['kiva_filters']['kiva_loans_lender'] = array(
    '#type' => 'textfield',
    '#title' => t('Lender ID'),
    '#default_value' => variable_get('kiva_loans_lender', ''),
    '#description' => t('Filter by a particular lender\'s kiva ID.'),
  );
  $form['kiva_loans_block']['kiva_filters']['kiva_filter_partner'] = array(
    '#type' => 'textfield',
    '#title' => t('Partner ID'),
    '#default_value' => variable_get('kiva_filter_partner', ''),
    '#description' => t('Filter by a particular lender\'s kiva ID.'),
  );
  $form['kiva_loans_block']['kiva_filters']['kiva_filter_gender'] = array(
    '#type' => 'select',
    '#title' => t('Gender'),
    '#description' => t('If supplied, results are filtered to loans with entrepreneurs of the specified gender. In the case of group loans, this matches against the predominate gender in the group.'),
    '#default_value' => variable_get('kiva_filter_gender', ''),
    '#options' => array (
      '' => 'All',
      'female' => 'Female',
      'male' => 'Male',
    ),
  );
  $form['kiva_loans_block']['kiva_filters']['kiva_filter_sector'] = array(
    '#type' => 'select',
    '#title' => t('Sector'),
    '#description' => t('A list of business sectors for which to filter results.'),
    '#default_value' => variable_get('kiva_filter_sector', ''),
    '#options' => array (
      '' => 'All',
      'agriculture' => 'Agriculture',
      'arts' => 'Arts',
      'clothing' => 'Clothing',
      'construction' => 'Construction',
      'education' => 'Education',
      'entertainment' => 'Entertainment',
      'food' => 'Food',
      'health' => 'Health',
      'housing' => 'Housing',
      'manufacturing' => 'Manufacturing',
      'personal+use' => 'Personal Use',
      'retail' => 'Retail',
      'services' => 'Services',
      'transportation' => 'Transportation',
      'wholesale' => 'Wholesale',
      'green' => 'Green',
    ),
  );
  $form['kiva_loans_block']['kiva_filters']['kiva_filter_status'] = array(
    '#type' => 'select',
    '#title' => t('Loan status'),
    '#description' => t('The status of loans to return.'),
    '#default_value' => variable_get('kiva_filter_status', 'fundraising'),
    '#options' => array (
      'fundraising' => 'Actively fundrasing',
      'funded' => 'Fully funded',
      'in_repayment' => 'In repayment',
      'paid' => 'Fully paid',
      'defaulted' => 'Defaulted',
    ),
  );
  $form['kiva_loans_block']['kiva_filters']['kiva_filter_region'] = array(
    '#type' => 'select',
    '#title' => t('Loan region'),
    '#description' => t('If supplied, results are filtered to loans only from the specified regions.'),
    '#default_value' => variable_get('kiva_filter_region', ''),
    '#options' => array (
      ''   => 'All regions',
      'na' => 'North America',
      'ca' => 'Central America',
      'sa' => 'South America',
      'af' => 'Africa',
      'as' => 'Asia',
      'me' => 'Middle East',
      'ee' => 'Eastern Europe',
    ),
  );
  $form['kiva_loans_block']['kiva_filters']['kiva_filter_sort_by'] = array(
    '#type' => 'select',
    '#title' => t('Sort order'),
    '#description' => t('The order by which to sort loans.'),
    '#default_value' => variable_get('kiva_filter_sort_by', ''),
    '#options' => array (
      'newest'   => 'Newest',
      'popularity' => 'Popularity',
      'loan_amount' => 'Loan Amount',
      'oldest' => 'Oldest',
      'expiration' => 'Expiration',
      'amount_remaining' => 'Amount Remaining',
      'repayment_term' => 'Repayment Term',
    ),
  );
  $form['kiva_loans_block']['kiva_filters']['kiva_filter_has_currency_loss'] = array(
    '#type' => 'checkbox',
    '#title' => t('Has Currency Loss'),
    '#default_value' => variable_get('kiva_filter_has_currency_loss', 0),
    '#description' => t('If true, only returns loans for which lenders have lost value due to fluctuations in currency exchange. This limits results to loans which are in repayment, paid, or defaulted.'),
  );
  $form['kiva_loans_block']['kiva_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
    '#description' => t('Choose which fields to display with each item.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE, 
  );
  $form['kiva_loans_block']['kiva_fields']['kiva_loans_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Image'),
    '#default_value' => variable_get('kiva_loans_image', 0),
    '#description' => t('Display image of borrower.'),
  );
  $form['kiva_loans_block']['kiva_fields']['kiva_loans_country'] = array(
    '#type' => 'checkbox',
    '#title' => t('Country'),
    '#default_value' => variable_get('kiva_loans_country', 0),
    '#description' => t('Display country of borrower.'),
  );
  $form['kiva_loans_block']['kiva_fields']['kiva_loans_amount'] = array(
    '#type' => 'checkbox',
    '#title' => t('Loan amount'),
    '#default_value' => variable_get('kiva_loans_amount', 0),
    '#description' => t('Display total amount of loan.'),
  );
  $form['kiva_loans_block']['kiva_fields']['kiva_loans_funded'] = array(
    '#type' => 'checkbox',
    '#title' => t('Funded'),
    '#default_value' => variable_get('kiva_loans_funded', 0),
    '#description' => t('Display amount of loan funded so far.'),
  );
  $form['kiva_loans_block']['kiva_fields']['kiva_loans_use'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use'),
    '#default_value' => variable_get('kiva_loans_use', 0),
    '#description' => t('Display desired use of loan.'),
  );
  $form['kiva_loans_block']['kiva_fields']['kiva_loans_sector'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sector'),
    '#default_value' => variable_get('kiva_loans_sector', 0),
    '#description' => t('Display economic sector of loan.'),
  );
  return system_settings_form($form);
}